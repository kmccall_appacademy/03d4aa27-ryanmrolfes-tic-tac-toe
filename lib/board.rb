class Board
  attr_reader :grid, :marks
  def initialize(board = nil)
    @grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]] if board.nil?
    # @grid = Array.new(3, Array.new(3, nil))
    @grid = board if board != nil
    @marks = [:X, :O]
  end

  def place_mark(position, mark)
    raise "position isn't empty" if @grid[position[0]][position[1]] != nil
    @grid[position[0]][position[1]] = mark
  end

  def empty?(position)
    return true if @grid[position[0]][position[1]].nil?
    false
  end

  def winner
    return :O if win_row?(:O)
    return :X if win_row?(:X)
    return :O if win_diag?(:O)
    return :X if win_diag?(:X)
    return :O if win_column?(:O)
    return :X if win_column?(:X)
    nil
  end

  def over?
    return true if winner != nil
    if @grid.flatten.include?(nil)
      return false
    else
      return true
    end
  end

  def win_row?(sym)
    return true if @grid.any? { |arr| arr == [sym, sym, sym] }
  end

  def win_diag?(sym)
    return true if @grid[0][0] == sym && @grid[1][1] == sym && @grid[2][2] == sym
    return true if @grid[0][2] == sym && @grid[1][1] == sym && @grid[2][0] == sym
    nil
  end

  def win_column?(sym)
    return true if @grid[0][0] == sym && @grid[1][0] == sym && @grid[2][0] == sym
    return true if @grid[0][1] == sym && @grid[1][1] == sym && @grid[2][1] == sym
    return true if @grid[0][2] == sym && @grid[1][2] == sym && @grid[2][2] == sym
    nil
  end
end
