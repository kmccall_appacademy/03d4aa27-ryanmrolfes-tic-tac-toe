# rspec spec/computer_player_spec.rb
#
# display should store the board it's passed as an instance variable,
# so that get_move has access to it
# get_move should return a winning move if one is available, and
# otherwise move randomly

class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = empty_spaces
    moves.each do |move|
      return move if wins?(move)
    end
    moves[0]
  end

  def wins?(move)
    board.grid[move[0]][move[1]] = mark
    if board.winner == mark
      board.grid[move[0]][move[1]] = nil
      true
    else
      board.grid[move[0]][move[1]] = nil
      false
    end
  end


  def empty_spaces
    moves = []
    moves << [0, 0] if board.grid[0][0] == nil
    moves << [0, 1] if board.grid[0][1] == nil
    moves << [0, 2] if board.grid[0][2] == nil
    moves << [1, 0] if board.grid[1][0] == nil
    moves << [1, 1] if board.grid[1][1] == nil
    moves << [1, 2] if board.grid[1][2] == nil
    moves << [2, 0] if board.grid[2][0] == nil
    moves << [2, 1] if board.grid[2][1] == nil
    moves << [2, 2] if board.grid[2][2] == nil
    moves
  end
end
