# rspec spec/game_spec.rb
#
# current_player
# switch_players!
# play_turn, which handles the logic for a single turn
# play, which calls play_turn each time through a loop until the game is over

# WHEN SETTING UP THE FINAL "GAME" YOU ARE PUTTING THE FORMERLY MADE
#PIECES TOGETHER. THINK ABOUT HOW THEY WORK AND WHAT NEEDS TO HAPPEN.
#
require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :current_player, :player1, :player2, :board

  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    player1.mark = :X
    player2.mark = :O
    @current_player = player1
    @board = Board.new
  end
#
  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end

  def switch_players!
    if @current_player == player1
      @current_player = player2
    else
      @current_player = player1
    end
  end
end
