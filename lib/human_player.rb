class HumanPlayer
  attr_reader :name, :mark
  def initialize(name, mark = nil)
    @name = name
    @mark = mark
  end

  def display(board)
    puts board.grid
  end

  def get_move
    p "Where move?"
    input = gets.chomp
    [input[0].to_i, input[-1].to_i]
  end

end
